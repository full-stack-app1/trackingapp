using System;
using System.Collections.Generic;
using Application.Services.Token;
using Application.Services.Vehicles;
using Domain.Entities;
using NSubstitute;
using NUnit.Framework;
using Persistence.Data.Interfaces;
using ServicesTests.Common;

namespace ServicesTests.VehicleTests
{
    [TestFixture]
    public class VehicleServiceTests
    {
        private IDbConnectionFactory dbConnectionFactory;
        private ITokenService tokenService;
        private RepositoriesHelper repositoriesHelper;
        private VehicleService vehicleService;

        [SetUp]
        protected void SetUp()
        {
            dbConnectionFactory = Substitute.For<IDbConnectionFactory>();
            tokenService = Substitute.For<ITokenService>();
            repositoriesHelper = new RepositoriesHelper(dbConnectionFactory, tokenService);
            vehicleService = repositoriesHelper.RegisterRepos().VehicleService;
        } 

        [Test]
        public void GIVEN_NewVehicle_WHEN_AddingToSystem_SHOULD_AddSuccessfully()
        {
            //-------------------------Arrange--------------------------
            var expectedResult = true;
            var valueToReturn = 1;

            //---------------------------Act-----------------------------
            QueryFirstAsync(dbConnectionFactory, null);
            ExecuteAsync(dbConnectionFactory, valueToReturn);
            var actualResult = vehicleService.CreateVehicleAsync(CreateVehicleData()).Result;

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult, actualResult.IsSuccess);
        }

        [Test]
        public void GIVEN_User_WHEN_RequestingForVehicles_SHOULD_ReturnListOfVehicles()
        {
            //-------------------------Arrange--------------------------
            var expectedResult = true;

            //---------------------------Act-----------------------------
            var valuesToReturn = CreateVehiclesList();
            QueryAsync(dbConnectionFactory, valuesToReturn);
            var actualResult = vehicleService.GetAllVehiclesAsync().Result;

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult, actualResult.IsSuccess);
            CollectionAssert.AreEqual(valuesToReturn, actualResult.Value);
        }

        [Test]
        public void GIVEN_User_WHEN_RequestingForSpecificVehicle_SHOULD_ReturnVehicle()
        {
            //-------------------------Arrange--------------------------
            var expectedResult = CreateVehicleData();
            var vehicleId = 5;

            //---------------------------Act-----------------------------
            QueryFirstAsync(dbConnectionFactory, CreateVehicleData());
            var actualResult = vehicleService.GetVehicleByIdAsync(vehicleId).Result;

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult.Brand, actualResult.Value.Brand);
        }

        [Test]
        public void GIVEN_VehicleIdThatDontExist_WHEN_RequestingForVehicle_SHOULD_ReturnNull()
        {
            //-------------------------Arrange--------------------------
            var vehicleId = 255;

            //---------------------------Act-----------------------------
            QueryFirstAsync(dbConnectionFactory, null);
            var actionResult = vehicleService.GetVehicleByIdAsync(vehicleId);

            //---------------------------Assert--------------------------
            Assert.AreEqual(null, actionResult.Result.Value);
        }

        [Test]
        public void GIVEN_ExistingVehicle_WHEN_AddingNewVehicle_SHOULD_ReturnVehicleAlreadyExistErrorMesssage()
        {
            //-------------------------Arrange--------------------------
            var expectedResult = "Failed to register vehicle number: STRakk33Test";

            //---------------------------Act-----------------------------
            QueryFirstAsync(dbConnectionFactory, CreateVehicleData());
            var actionResult = vehicleService.CreateVehicleAsync(CreateVehicleData());

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult, actionResult.Result.Error);
            Assert.AreEqual(false, actionResult.Result.IsSuccess);
        }

        [Test]
        public void GIVEN_Vehicle_WHEN_UpdatingDetails_SHOULD_UpdateSuccessfully()
        { 
            //-------------------------Arrange--------------------------
            var expectedResult = true;
            var valueToReturn = 1;

            //---------------------------Act-----------------------------
            ExecuteAsync(dbConnectionFactory, valueToReturn);
            var actualResult = vehicleService.UpdateVehicleAsync(CreateVehicleData());

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult, actualResult.Result.Value);
        }

        [Test]
        public void GIVEN_VehicleId_WHEN_DeletingVehicle_SHOULD_DeleteSuccessfully()
        {
            //-------------------------Arrange--------------------------
            var expectedResult = true;
            var valueToReturn = 1;
            var vehicleId = 5;

            //---------------------------Act-----------------------------
            DeleteAsync(dbConnectionFactory, valueToReturn);
            var actualResult = vehicleService.DeleteVehicleAsync(vehicleId);

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult, actualResult.Result.Value);
        }

        [Test]
        public void GIVEN_NotExistingVehicle_WHEN_DeletingVehicle_SHOULD_ReturnFalse()
        {
            //-------------------------Arrange--------------------------
            var expectedResult = false;
            var valueToReturn = 0;
            var vehicleId = 5;

            //---------------------------Act-----------------------------
            DeleteAsync(dbConnectionFactory, valueToReturn);
            var actualResult = vehicleService.DeleteVehicleAsync(vehicleId);

            //---------------------------Assert--------------------------
            Assert.AreEqual(expectedResult, actualResult.Result.Value);
        }

        private static void ExecuteAsync(IDbConnectionFactory dbConnectionFactory, int valueToReturn)
        {
            dbConnectionFactory.ExecuteAsync<Vehicle>(Arg.Any<string>(), Arg.Any<Vehicle>()).Returns(valueToReturn);
        }

        private static void QueryAsync(IDbConnectionFactory dbConnectionFactory, List<Vehicle> valuesToReturn)
        {
            dbConnectionFactory.QueryAsync<Vehicle>(Arg.Any<string>()).Returns(valuesToReturn);
        }

        private static void QueryFirstAsync(IDbConnectionFactory dbConnectionFactory, Vehicle valueToReturn)
        {
            dbConnectionFactory.QueryFirstAsync<Vehicle>(Arg.Any<string>()).Returns(valueToReturn);
        }

        private static void DeleteAsync(IDbConnectionFactory dbConnectionFactory, int valueToReturn)
        {
            dbConnectionFactory.DeleteAsync(Arg.Any<string>(), Arg.Any<int>()).Returns(valueToReturn);
        }

        private static Vehicle CreateVehicleData()
        {
           var vehicle = new Vehicle
           {
                GuidId = Guid.NewGuid(),
                AppUserId = 2,
                RegistrationDate = DateTime.Now,
                Transmission = "Manual",
                Brand = "Lamborghini",
                Model = "Urus",
                Color = "Blue",
                FuelType = "Gasoline",
                EngineNumber = "aa55aaTest",
                NumberPlate = "STRakk33Test",
                Engine = 2535.0M,
                Description = "Car Test",
                VIN = "Test123",
           };

           return vehicle;
        }

        private static List<Vehicle> CreateVehiclesList()
        {
            var vehicleOne = new Vehicle() 
            {
                Id = 1,
                AppUserId = 2,
                GuidId = Guid.NewGuid(),
                Brand = "Toyota",
                Model = "Yaris",
                Mileage = "25000 KM",
                Color = "Grey",
                FuelType = "Gasoline",
                Transmission = "Manual",
                Engine = 64646.6M,
                VIN = "TEST-VIN-NUMBER",
                EngineNumber = "TEST-ENGINE-NUMBER",
                NumberPlate = "TEST-NUMBER-PLATE",
                Description = "Car Test",
                RegistrationDate =  DateTime.Now
            };

            var vehicleTwo = new Vehicle() 
            {
                Id = 2,
                AppUserId = 3,
                GuidId = Guid.NewGuid(),
                Brand = "Hyundai",
                Model = "i20",
                Mileage = "32000 KM",
                Color = "White",
                FuelType = "Petrol",
                Transmission = "Automatic",
                Engine = 3256.6M,
                VIN = "TEST-VIN-NUMBER",
                EngineNumber = "TEST-ENGINE-NUMBER",
                NumberPlate = "TEST-NUMBER-PLATE",
                Description = "Car Test",
                RegistrationDate =  DateTime.Now
            };
            
            
            return new List<Vehicle>() 
            {
                vehicleOne, vehicleTwo
            };
        }
    }
}