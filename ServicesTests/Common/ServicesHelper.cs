using Application.Services.AppUsers;
using Application.Services.RecoveredVehicles;
using Application.Services.StolenVehicles;
using Application.Services.StolenVehicleUpdates;
using Application.Services.Vehicles;

namespace ServicesTests.Common
{
    public class ServicesHelper
    {
        public AppUserService AppUserService { get; set; }
        public VehicleService VehicleService { get; set; }
        public RecoveredVehicleService RecoveredVehicleService { get; set; }
        public StolenVehicleService StolenVehicleService { get; set; }
        public StolenVehicleUpdateService StolenVehicleUpdateService { get; set; }
    }
}