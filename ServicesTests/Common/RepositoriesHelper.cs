using Application.Services.AppUsers;
using Application.Services.Encryptions;
using Application.Services.RecoveredVehicles;
using Application.Services.StolenVehicles;
using Application.Services.StolenVehicleUpdates;
using Application.Services.Token;
using Application.Services.Vehicles;
using Persistence.Data.Interfaces;
using Persistence.Data.Repositories;

namespace ServicesTests.Common
{
    public class RepositoriesHelper
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        private readonly ITokenService _tokenService;
        public RepositoriesHelper(IDbConnectionFactory dbConnectionFactory, ITokenService tokenService)
        {
            _tokenService = tokenService;
            _dbConnectionFactory = dbConnectionFactory;
        }

        public ServicesHelper RegisterRepos()
        {
            var appUserRepository = new AppUserRepository(_dbConnectionFactory);
            var vehicleRepository = new VehicleRepository(_dbConnectionFactory);
            var stolenVehicleRepository = new StolenVehicleRepository(_dbConnectionFactory);
            var stolenVehicleUpdateRepository = new StolenVehicleUpdateRepository(_dbConnectionFactory);
            var recoveredVehicleRepository = new RecoveredVehicleRepository(_dbConnectionFactory);
            var encryptionProvider = new EncryptionProvider();

            var unitOfWork = new UnitOfWork(appUserRepository, vehicleRepository, stolenVehicleRepository, stolenVehicleUpdateRepository, recoveredVehicleRepository);

            return new ServicesHelper
            {
                AppUserService = new AppUserService(unitOfWork, _tokenService, encryptionProvider),
                VehicleService = new VehicleService(unitOfWork),
                RecoveredVehicleService = new RecoveredVehicleService(unitOfWork),
                StolenVehicleService = new StolenVehicleService(unitOfWork),
                StolenVehicleUpdateService = new StolenVehicleUpdateService(unitOfWork)
            };
        }
    }
}