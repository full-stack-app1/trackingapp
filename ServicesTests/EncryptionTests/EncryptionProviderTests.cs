using System;
using System.Linq;
using Application.Services.Encryptions;
using NUnit.Framework;

namespace ServicesTests.EncryptionTests
{
    [TestFixture]
    public class EncryptionProviderTests
    {
        private IEncryptionProvider encryptionProvider;

        [SetUp]
        protected void TestSetUp()
        {
            encryptionProvider = new EncryptionProvider();
        }

        [Test]
        public void GIVEN_PasswordSaltAndUserEnteredPassword_WHEN_DecryptingPassword_SHOULD_ReturnCorrectPasswordHash()
        {
            //--------------Arrange--------------------
            var password = "password";

            //--------------Act------------------------
            var encryptedData = encryptionProvider.EncryptData(password);
            var passwordHash = encryptionProvider.DecryptData(encryptedData.Salt, password);

            //-------------Assert----------------------
            Assert.True(passwordHash.SequenceEqual(encryptedData.EncryptionHash));
        }

        [Test]
        public void GIVEN_Password_WHEN_EncryptingPassword_SHOULD_ReturnPasswordHash()
        {
            //--------------Arrange--------------------
            var password = "password";
            var expectedHashLength = 64;
            var expectedSaltLength = 128;

            //--------------Act------------------------
            var actualResult = encryptionProvider.EncryptData(password);

            //-------------Assert----------------------
            Assert.IsNotNull(actualResult.EncryptionHash);
            Assert.IsNotNull(actualResult.Salt);
            Assert.AreEqual(expectedHashLength, actualResult.EncryptionHash.Length);
            Assert.AreEqual(expectedSaltLength, actualResult.Salt.Length);
        }
    }
}