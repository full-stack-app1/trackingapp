using System;
using System.Text;
using Application.Core;
using Application.DTOs;
using Application.Services.AppUsers;
using Application.Services.Token;
using Domain.Entities;
using NSubstitute;
using NUnit.Framework;
using Persistence.Data.Interfaces;
using ServicesTests.Common;

namespace ServicesTests.AppUserTests
{
    [TestFixture]
    public class AppUserServiceTests
    {
        private IDbConnectionFactory dbConnectionFactory;
        private ITokenService tokenService;
        private RepositoriesHelper repositoriesHelper;
        private AppUserService appUserService;

        [SetUp]
        protected void SetUp()
        {
            dbConnectionFactory = Substitute.For<IDbConnectionFactory>();
            tokenService = Substitute.For<ITokenService>();
            repositoriesHelper = new RepositoriesHelper(dbConnectionFactory, tokenService);
            appUserService = repositoriesHelper.RegisterRepos().AppUserService;
        }

        [Test]
        public void GIVEN_UserCorrectLoginDetails_WHEN_LogingIn_SHOULD_ReturnToken()
        {
            //------------Arrange---------------------
            var expectedResult = CreateLoginData().UserName;

            //------------Act-------------------------
            QueryFirstAsync(dbConnectionFactory, CreateAppUserDataWithCorrectPasswordHash());
            var result = appUserService.Login(CreateLoginData()).Result;

            //------------Assert-----------------------
            Assert.AreEqual(expectedResult, result.Value.Username);
            Assert.True(result.IsSuccess);
            Assert.NotNull(result.Value.Token);
        }

        [Test]
        public void GIVEN_UserEntersWrongPassword_WHEN_LogingIn_SHOULD_ReturnPasswordError()
        {
            //------------Arrange---------------------
            var expectedResult = Constants.PasswordError;

            //------------Act-------------------------
            QueryFirstAsync(dbConnectionFactory, CreateAppUserData());
            var result = appUserService.Login(CreateLoginData()).Result;

            //------------Assert-----------------------
            Assert.AreEqual(expectedResult, result.Error);
            Assert.False(result.IsSuccess);
            Assert.Null(result.Value);
        }

        [Test]
        public void GIVEN_UserEntersNotExistingUsername_WHEN_LogingIn_SHOULD_ReturnUsernameError()
        {
            //------------Arrange---------------------
            var expectedResult = Constants.UsernameError;

            //------------Act-------------------------
            QueryFirstAsync(dbConnectionFactory);
            var result = appUserService.Login(CreateLoginData()).Result;

            //------------Assert-----------------------
            Assert.AreEqual(expectedResult, result.Error);
            Assert.False(result.IsSuccess);
            Assert.Null(result.Value);
        }

        [Test]
        public void GIVEN_UserEntersCorrectDetails_WHEN_Registering_SHOULD_RegisterSuccessfully()
        {
            //------------Arrange---------------------
            var expectedResult = true;
            var valueToReturn = 1;

            //------------Act-------------------------
            ExecuteAsync(dbConnectionFactory, valueToReturn);
            var result = appUserService.Register(CreateRegisterDto()).Result;

            //------------Assert-----------------------
            Assert.AreEqual(expectedResult, result.IsSuccess);
            Assert.True(result.Value);
        }

        [Test]
        public void GIVEN_UserEntersExistingUsername_WHEN_Registering_SHOULD_ReturnUsernameError()
        {
            //------------Arrange---------------------
            var expectedResult = $"{Constants.RegisterError} as username is taken";
            var valueToReturn = 1;

            //------------Act-------------------------
            QueryFirstAsync(dbConnectionFactory, CreateAppUserData());
            ExecuteAsync(dbConnectionFactory, valueToReturn);
            var result = appUserService.Register(CreateRegisterDto()).Result;

            //------------Assert-----------------------
            Assert.AreEqual(expectedResult, result.Error);
        }

        private static void QueryFirstAsync(IDbConnectionFactory dbConnectionFactory, AppUser returnValue = null)
        {
            dbConnectionFactory.QueryFirstAsync<AppUser>(Arg.Any<string>())
                .Returns(returnValue);
        }

        private static void ExecuteAsync(IDbConnectionFactory dbConnectionFactory, int returnValue)
        {
            dbConnectionFactory.ExecuteAsync<AppUser>(Arg.Any<string>(), Arg.Any<AppUser>())
                .Returns(returnValue);
        }

        private static AppUser CreateAppUserDataWithCorrectPasswordHash()
        {
           var appuser = new AppUser
           {
                FirstName = "Bob",
                LastName = "Bobbity",
                Gender = "Male",
                PhoneNumber = "0744733721",
                Email = "bob@test.com",
                RegistrationDate = DateTime.Now,
                UserName = "bob",
                PasswordHash = new byte[] { 222, 196, 112, 214, 104, 197, 60, 186, 33, 213, 59, 254, 123, 151, 70, 103, 207, 116, 91, 85, 206, 48, 170, 244, 29, 52, 214, 33, 159, 221, 123, 209, 236, 125, 160, 224, 64, 188, 110, 88,149, 121, 68, 190, 51, 59, 20, 101, 34, 77, 89, 215, 8, 160, 118, 125, 162, 184, 28, 92, 214, 87, 25, 194},
                PasswordSalt = Encoding.ASCII.GetBytes("OphN54i5aaodI6FyBxBVgvv9Q4XQStiNvL2OZtKapqZrHV95PKHeQUzN/nzLMUEgGF1P7rlJHboz3PiZg+mwA9IcBRVEaUK/gkyq+11PS1gye5qzIOfbBwY0V9m4GK1ekUczl8Xrgea1Mul53KF3RJbHCSmU7LsB81EkzIKvADM=")
           };

           return appuser;
        }

         private static AppUser CreateAppUserData()
        {
           var appuser = new AppUser
           {
                FirstName = "Bob",
                LastName = "Bobbity",
                Gender = "Male",
                PhoneNumber = "0744733721",
                Email = "bob@test.com",
                RegistrationDate = DateTime.Now,
                UserName = "bob",
                PasswordHash = Encoding.ASCII.GetBytes("TTNOU2R6RnRha1pRVEc5b01WUjJLMlUxWkVkYU9Ea3dWekZZVDAxTGNqQklWRlJYU1ZvdlpHVTVTSE5tWVVSblVVeDRkVmRLVmpWU1REUjZUM2hTYkVsck1Wb3hkMmxuWkc0eWFYVkNlR014YkdOYWQyYzlQUT09"),
                PasswordSalt = Encoding.ASCII.GetBytes("OphN54i5aaodI6FyBxBVgvv9Q4XQStiNvL2OZtKapqZrHV95PKHeQUzN/nzLMUEgGF1P7rlJHboz3PiZg+mwA9IcBRVEaUK/gkyq+11PS1gye5qzIOfbBwY0V9m4GK1ekUczl8Xrgea1Mul53KF3RJbHCSmU7LsB81EkzIKvADM=")
           };

           return appuser;
        }

        private static LoginDto CreateLoginData()
        {
            return new LoginDto
            {
                UserName = "bob",
                Password = "password"
            };
        }

        private static RegisterDto CreateRegisterDto()
        {
            var registerDto = new RegisterDto
            {
                FirstName = "Bob",
                LastName = "Bobbity",
                Gender = "M",
                PhoneNumber = "0744733729",
                Email = "bob@test.com",
                UserName = "Bob",
                Password = "password"
            };

            return registerDto;
        }
    }
}