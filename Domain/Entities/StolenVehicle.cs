using System;

namespace Domain.Entities
{
    public class StolenVehicle : BaseEntity
    {
        public int VehicleId { get; set; }
        public string Location { get; set; }
        public DateTime DateStolen { get; set; }
        public string Description { get; set; }
    }
}