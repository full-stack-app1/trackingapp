using System;

namespace Domain.Entities
{
    public class Vehicle : BaseEntity
    {
        public int AppUserId { get; set; }
        public Guid GuidId { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Mileage { get; set; }
        public string Color { get; set; }
        public string FuelType { get; set; }
        public string Transmission { get; set; }
        public decimal Engine { get; set; }
        public string VIN { get; set; }
        public string EngineNumber { get; set; }
        public string NumberPlate { get; set; }
        public string Description { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}