using System;

namespace Domain.Entities
{
    public class RecoveredVehicle : BaseEntity
    {
        public int StolenVehicleId { get; set; }
        public int VehicleId { get; set; }
        public string Location { get; set; }
        public DateTime DateRecovered { get; set; }
        public string Description { get; set; }
    }
}