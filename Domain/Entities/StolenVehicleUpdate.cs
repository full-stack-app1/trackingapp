using System;

namespace Domain.Entities
{
    public class StolenVehicleUpdate : BaseEntity
    {
        public int StolenVehicleId { get; set; }
        public int VehicleId { get; set; }
        public string Location { get; set; }
        public string comment { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}