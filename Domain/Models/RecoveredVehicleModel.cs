using System;

namespace Domain.Models
{
    public class RecoveredVehicleModel
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Vin { get; set; }
        public string EngineNumber { get; set; }
        public string NumberPlate { get; set; }
        public string Location { get; set; }
        public DateTime DateRecovered { get; set; }
        public string Description { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}