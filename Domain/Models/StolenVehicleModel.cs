using System;

namespace Domain.ViewModels
{
    public class StolenVehicleModel
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Vin { get; set; }
        public string EngineNumber { get; set; }
        public string NumberPlate { get; set; }
        public string Location { get; set; }
        public DateTime DateStolen { get; set; }
        public string Description { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}