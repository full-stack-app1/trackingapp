using Domain.Entities;
using FluentValidation;

namespace Application.Services.Vehicles
{
    public class VehicleValidator : AbstractValidator<Vehicle>
    {
        public VehicleValidator()
        {
            RuleFor(x => x.Brand).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Model).NotEmpty();
            RuleFor(x => x.VIN).NotEmpty();
            RuleFor(x => x.EngineNumber).NotEmpty();
            RuleFor(x => x.NumberPlate).NotEmpty();
            RuleFor(x => x.Mileage).NotEmpty();
            RuleFor(x => x.Color).NotEmpty();
            RuleFor(x => x.Transmission).NotEmpty();
        }
    }
}