using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;
using PeanutButter.RandomGenerators;
using Persistence.Data.Interfaces;

namespace Application.Services.Vehicles
{
    public class VehicleService : IVehicleService
    {
        private readonly IUnitOfWork _unitOfWork;
        public VehicleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Result<bool>> CreateVehicleAsync(Vehicle vehicle)
        {
            vehicle.RegistrationDate = DateTime.Now;
            vehicle.GuidId = Guid.NewGuid();
            //TODO: This should be configured to come from AuthToken
            vehicle.AppUserId = RandomValueGen.GetRandomInt(1, 6);

            var result = await _unitOfWork.Vehicles.GetVehicleByNumberPlateAsync(vehicle.NumberPlate);
            
            if (result != null)
                return Result<bool>.Failure($"{Constants.RegisterError} vehicle number: {vehicle.NumberPlate}");
                
            return Result<bool>.Success(await _unitOfWork.Vehicles.AddAsync(vehicle) > 0);
        }

        public async Task<Result<bool>> DeleteVehicleAsync(int id)
        {
            return Result<bool>.Success(await _unitOfWork.Vehicles.DeleteAsync(id) > 0);
        }
        
        public async Task<Result<List<Vehicle>>> GetAllVehiclesAsync()
        {
            return Result<List<Vehicle>>.Success(await _unitOfWork.Vehicles.QueryAsync());
        }

        public async Task<Result<Vehicle>> GetVehicleByIdAsync(int id)
        {
            return Result<Vehicle>.Success(await _unitOfWork.Vehicles.QueryFirstAsync(id));
        }

        public async Task<Result<bool>> UpdateVehicleAsync(Vehicle vehicleToUpdate)
        {
            return Result<bool>.Success(await _unitOfWork.Vehicles.UpdateAsync(vehicleToUpdate) > 0);
        }
    }
}