using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;

namespace Application.Services.Vehicles
{
    public interface IVehicleService
    {
        Task<Result<bool>> CreateVehicleAsync(Vehicle vehicle);
        Task<Result<bool>> DeleteVehicleAsync(int id);
        Task<Result<Vehicle>> GetVehicleByIdAsync(int id);
        Task<Result<bool>> UpdateVehicleAsync(Vehicle vehicle);
        Task<Result<List<Vehicle>>> GetAllVehiclesAsync();
    }
}