using System;
using System.Linq;
using System.Threading.Tasks;
using Application.Core;
using Application.DTOs;
using Application.Services.Encryptions;
using Application.Services.Token;
using Domain.Entities;
using Persistence.Data.Interfaces;

namespace Application.Services.AppUsers
{
    public class AppUserService : IAppUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITokenService _tokenService;
        private readonly IEncryptionProvider _encryptionProvider;
        public AppUserService(IUnitOfWork unitOfWork, ITokenService tokenService, IEncryptionProvider encryptionProvider)
        {
            _encryptionProvider = encryptionProvider;
            _tokenService = tokenService;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<UserDto>> Login(LoginDto loginDto)
        {
            var user = await UserExists(loginDto.UserName);

            if (user == null)
                return Result<UserDto>.Failure(Constants.UsernameError);

            var computedHash = _encryptionProvider.DecryptData(user.PasswordSalt, loginDto.Password);

            if (!computedHash.SequenceEqual(user.PasswordHash))
                return Result<UserDto>.Failure(Constants.PasswordError);

            return Result<UserDto>.Success(new UserDto
            {
                Username = user.UserName,
                Token = _tokenService.CreateToken(user)
            });
        }

        public async Task<Result<bool>> Register(RegisterDto registerDto)
        {
            if (await UserExists(registerDto.UserName) != null)
                return Result<bool>.Failure($"{Constants.RegisterError} as username is taken");
            
            var encryptedData = _encryptionProvider.EncryptData(registerDto.Password);

            var appUser = new AppUser
            {
                FirstName = registerDto.FirstName,
                LastName = registerDto.LastName,
                Gender = registerDto.Gender,
                PhoneNumber = registerDto.PhoneNumber,
                Email = registerDto.Email,
                RegistrationDate = DateTime.Now,
                UserName = registerDto.UserName.ToLower(),
                PasswordHash = encryptedData.EncryptionHash,
                PasswordSalt = encryptedData.Salt
            };

            return Result<bool>.Success(await _unitOfWork.AppUsers.AddAsync(appUser) > 0);
        }

        private async Task<AppUser> UserExists(string username)
        {
            return await _unitOfWork.AppUsers.FindUserAsync(username.ToLower());
        }
    }
}