using Application.DTOs;
using FluentValidation;

namespace Application.Services.AppUsers
{
    public class AppUserValidator : AbstractValidator<RegisterDto>
    {
        public AppUserValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
            RuleFor(x => x.Gender).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.UserName).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}