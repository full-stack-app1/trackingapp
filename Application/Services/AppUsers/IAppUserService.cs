using System.Threading.Tasks;
using Application.Core;
using Application.DTOs;

namespace Application.Services.AppUsers
{
    public interface IAppUserService
    {
        Task<Result<bool>> Register(RegisterDto registerDto);
        Task<Result<UserDto>> Login(LoginDto loginDto);
    }
}