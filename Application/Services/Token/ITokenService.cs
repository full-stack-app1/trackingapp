using Domain.Entities;

namespace Application.Services.Token
{
    public interface ITokenService
    {
        string CreateToken(AppUser appUser);
    }
}