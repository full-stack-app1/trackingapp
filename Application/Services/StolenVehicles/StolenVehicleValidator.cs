using Domain.Entities;
using FluentValidation;

namespace Application.Services.StolenVehicles
{
    public class StolenVehicleValidator : AbstractValidator<StolenVehicle>
    {
        public StolenVehicleValidator()
        {
            RuleFor(x => x.Location).NotEmpty();
            RuleFor(x => x.DateStolen).NotEmpty();
        }
    }
}