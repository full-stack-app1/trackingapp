using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;
using Domain.ViewModels;

namespace Application.Services.StolenVehicles
{
    public interface IStolenVehicleService
    {
        Task<Result<bool>> ReportStolenVehicleAsync(StolenVehicle stolenVehicle);
        Task<Result<StolenVehicleModel>> GetStolenVehicleByNumberPlate(string numberPlate);
        Task<Result<List<StolenVehicleModel>>> GetStolenVehiclesAsync();
    }
}