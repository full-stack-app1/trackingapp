using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;
using Domain.ViewModels;
using Persistence.Data.Interfaces;

namespace Application.Services.StolenVehicles
{
    public class StolenVehicleService : IStolenVehicleService
    {
        private readonly IUnitOfWork _unitOfWork;
        public StolenVehicleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<StolenVehicleModel>> GetStolenVehicleByNumberPlate(string numberPlate)
        {
            return Result<StolenVehicleModel>.Success(await _unitOfWork.StolenVehicles.GetStolenVehicleDetailsByNumberPlate(numberPlate));
        }

        public async Task<Result<List<StolenVehicleModel>>> GetStolenVehiclesAsync()
        {
            return Result<List<StolenVehicleModel>>.Success(await _unitOfWork.StolenVehicles.GetAllStolenVehicles());
        }

        public async Task<Result<bool>> ReportStolenVehicleAsync(StolenVehicle stolenVehicle)
        {
            return Result<bool>.Success(await _unitOfWork.StolenVehicles.AddAsync(stolenVehicle) > 0);
        }
    }
}