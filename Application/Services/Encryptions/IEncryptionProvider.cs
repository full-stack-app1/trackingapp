
namespace Application.Services.Encryptions
{
    public interface IEncryptionProvider
    {
        byte[] DecryptData(byte[] salt, string stringToDecrypt);
        EncryptedData EncryptData(string stringToEncrypt);
    }
}