namespace Application.Services.Encryptions
{
    public class EncryptedData
    {
        public byte[] EncryptionHash { get; set; }
        public byte[] Salt { get; set; }
    }
}