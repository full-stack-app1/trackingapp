using System.Security.Cryptography;
using System.Text;

namespace Application.Services.Encryptions
{
    public class EncryptionProvider : IEncryptionProvider
    {

        public byte[] DecryptData(byte[] salt, string stringToDecrypt)
        {
            using var hmacEncryption = new HMACSHA512(salt);

            return hmacEncryption.ComputeHash(Encoding.UTF8.GetBytes(stringToDecrypt));
        }

        public EncryptedData EncryptData(string stringToEncrypt)
        {
            using var hmacEncryption = new HMACSHA512();

            return new EncryptedData
            {
               EncryptionHash = hmacEncryption.ComputeHash(Encoding.UTF8.GetBytes(stringToEncrypt)),
               Salt = hmacEncryption.Key
            };
        }
    }
}