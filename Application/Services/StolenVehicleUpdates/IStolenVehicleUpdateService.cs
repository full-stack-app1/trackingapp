using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;

namespace Application.Services.StolenVehicleUpdates
{
    public interface IStolenVehicleUpdateService
    {
        Task<Result<bool>> AddVehicleUpdateAysnc(StolenVehicleUpdate stolenVehicleUpdate);
        Task<Result<bool>> DeleteVehicleUpdateAsync(int id);
        Task<Result<List<StolenVehicleUpdate>>> GetVehicleUpdatesAsync();
    }
}