using Domain.Entities;
using FluentValidation;

namespace Application.Services.StolenVehicleUpdates
{
    public class StolenVehicleUpdateValidator : AbstractValidator<StolenVehicleUpdate>
    {
        public StolenVehicleUpdateValidator()
        {
            RuleFor(x => x.comment).NotEmpty();
            RuleFor(x => x.UpdateDate).NotEmpty();
        }
    }
}