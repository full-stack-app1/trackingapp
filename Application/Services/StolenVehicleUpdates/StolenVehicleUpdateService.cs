using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;
using Persistence.Data.Interfaces;

namespace Application.Services.StolenVehicleUpdates
{
    public class StolenVehicleUpdateService : IStolenVehicleUpdateService
    {
        private readonly IUnitOfWork _unitOfWork;
        public StolenVehicleUpdateService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<bool>> AddVehicleUpdateAysnc(StolenVehicleUpdate stolenVehicleUpdate)
        {
            return Result<bool>.Success(await _unitOfWork.StolenVehicleUpdates.AddAsync(stolenVehicleUpdate) > 0);
        }

        public async Task<Result<bool>> DeleteVehicleUpdateAsync(int id)
        {
            return Result<bool>.Success(await _unitOfWork.StolenVehicleUpdates.DeleteAsync(id) > 0);
        }

        public async Task<Result<List<StolenVehicleUpdate>>> GetVehicleUpdatesAsync()
        {
            return Result<List<StolenVehicleUpdate>>.Success(await _unitOfWork.StolenVehicleUpdates.QueryAsync());
        }
    }
}