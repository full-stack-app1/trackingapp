using Domain.Entities;
using FluentValidation;

namespace Application.Services.RecoveredVehicles
{
    public class RecoveredVehicleValidator : AbstractValidator<RecoveredVehicle>
    {
        public RecoveredVehicleValidator()
        {
            RuleFor(x => x.Location).NotEmpty();
            RuleFor(x => x.DateRecovered).NotEmpty();
        }
    }
}