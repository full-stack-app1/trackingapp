using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;
using Domain.Models;

namespace Application.Services.RecoveredVehicles
{
    public interface IRecoveredVehicleService
    {
        Task<Result<bool>> AddRecoveredVehicleAsync(RecoveredVehicle recoveredVehicle);
        Task<Result<RecoveredVehicleModel>> GetRecoveredVehicle(string numberPlate);
        Task<Result<List<RecoveredVehicleModel>>> GetAllRecoveredVehicles();
    }
}