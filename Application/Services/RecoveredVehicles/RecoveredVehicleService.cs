using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Core;
using Domain.Entities;
using Domain.Models;
using Persistence.Data.Interfaces;

namespace Application.Services.RecoveredVehicles
{
    public class RecoveredVehicleService : IRecoveredVehicleService
    {
        private readonly IUnitOfWork _unitOfWork;
        public RecoveredVehicleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<bool>> AddRecoveredVehicleAsync(RecoveredVehicle recoveredVehicle)
        {
            return Result<bool>.Success(await _unitOfWork.RecoveredVehicles.AddAsync(recoveredVehicle) > 0);
        }

        public async Task<Result<RecoveredVehicleModel>> GetRecoveredVehicle(string numberPlate)
        {
            return Result<RecoveredVehicleModel>.Success(await _unitOfWork.RecoveredVehicles.GetRecoveredVehicleDetailsByNumberPlate(numberPlate));
        }

        public async Task<Result<List<RecoveredVehicleModel>>> GetAllRecoveredVehicles()
        {
            return Result<List<RecoveredVehicleModel>>.Success(await _unitOfWork.RecoveredVehicles.GetAllRecoveredVehicles());
        }
    }
}