namespace Application.Core
{
    public static class Constants
    {
        public const string RegisterError = "Failed to register";
        public const string UsernameError = "Invalid username";
        public const string PasswordError = "Invalid password";
        public const string Key = "TokenKey";
    }
}