using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Persistence.Data.Interfaces;

namespace Persistence.Data
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly IConfiguration _config;
        public DbConnectionFactory(IConfiguration config)
        {
            _config = config;
        }

        public async Task<int> ExecuteAsync<T>(string query, T entity)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();
                var result = await connection.ExecuteAsync(query, entity);
                return result;
            }
        }

        public async Task<int> DeleteAsync(string query, int id)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();
                var result = await connection.ExecuteAsync(query, new {id = id});
                return result;
            }
        }

        public async Task<List<T>> QueryAsync<T>(string query)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();
                var result = await connection.QueryAsync<T>(query);
                return result.ToList();
            }
        }

        public async Task<T> QueryFirstAsync<T>(string query)
        {
            using (var connection = CreateConnection())
            {
                connection.Open();
                var result = await connection.QueryFirstOrDefaultAsync<T>(query);
                return result;
            }
        }

        private IDbConnection CreateConnection()
        {
            return new NpgsqlConnection(_config.GetConnectionString("DefaultConnection"));
        }
    }
}