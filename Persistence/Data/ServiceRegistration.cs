using Microsoft.Extensions.DependencyInjection;
using Persistence.Data.Interfaces;
using Persistence.Data.Repositories;

namespace Persistence.Data
{
    public static class ServiceRegistration
    {
        public static void AddPersistence(this IServiceCollection services)
        {
            services.AddTransient<IDbConnectionFactory, DbConnectionFactory>();
            services.AddTransient<IAppUserRepository, AppUserRepository>();
            services.AddTransient<IVehicleRepository, VehicleRepository>();
            services.AddTransient<IStolenVehicleRepository, StolenVehicleRepository>();
            services.AddTransient<IStolenVehicleUpdateRepository, StolenVehicleUpdateRepository>();
            services.AddTransient<IRecoveredVehicleRepository, RecoveredVehicleRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}