using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Models;

namespace Persistence.Data.Interfaces
{
    public interface IRecoveredVehicleRepository : IGenericRepository<RecoveredVehicle>
    {
        Task<RecoveredVehicleModel> GetRecoveredVehicleDetailsByNumberPlate(string numberPlate);
        Task<List<RecoveredVehicleModel>> GetAllRecoveredVehicles();
    }
}