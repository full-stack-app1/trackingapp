using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.ViewModels;

namespace Persistence.Data.Interfaces
{
    public interface IStolenVehicleRepository : IGenericRepository<StolenVehicle>
    {
        Task<StolenVehicleModel> GetStolenVehicleDetailsByNumberPlate(string numberPlate);
        Task<List<StolenVehicleModel>> GetAllStolenVehicles();
    }
}