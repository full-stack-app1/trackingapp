using System.Threading.Tasks;
using Domain.Entities;

namespace Persistence.Data.Interfaces
{
    public interface IVehicleRepository : IGenericRepository<Vehicle>
    {
        Task<Vehicle> GetVehicleByNumberPlateAsync(string numberPlate);
    }
}