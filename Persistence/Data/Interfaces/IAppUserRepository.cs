using System.Threading.Tasks;
using Domain.Entities;

namespace Persistence.Data.Interfaces
{
    public interface IAppUserRepository : IGenericRepository<AppUser>
    {
        Task<AppUser> FindUserAsync(string username);
    }
}