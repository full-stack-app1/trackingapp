using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;

namespace Persistence.Data.Interfaces
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<int> AddAsync(T entity);
        Task<int> UpdateAsync(T entity);
        Task<int> DeleteAsync(int id);
        Task<List<T>> QueryAsync();
        Task<T> QueryFirstAsync(int id);
    }
}