using System.Collections.Generic;
using System.Threading.Tasks;

namespace Persistence.Data.Interfaces
{
    public interface IDbConnectionFactory
    {
        Task<T> QueryFirstAsync<T>(string query);
        Task<List<T>> QueryAsync<T>(string query);
        Task<int> ExecuteAsync<T>(string query, T entity);
        Task<int> DeleteAsync(string query, int id);
    }
}