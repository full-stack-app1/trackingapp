using Domain.Entities;

namespace Persistence.Data.Interfaces
{
    public interface IStolenVehicleUpdateRepository : IGenericRepository<StolenVehicleUpdate>
    {
        
    }
}