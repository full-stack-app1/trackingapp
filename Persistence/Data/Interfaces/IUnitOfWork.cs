namespace Persistence.Data.Interfaces
{
    public interface IUnitOfWork
    {
        IAppUserRepository AppUsers { get; }
        IVehicleRepository Vehicles { get; }
        IStolenVehicleRepository StolenVehicles { get; }
        IStolenVehicleUpdateRepository StolenVehicleUpdates { get; }
        IRecoveredVehicleRepository RecoveredVehicles { get; }
    }
}