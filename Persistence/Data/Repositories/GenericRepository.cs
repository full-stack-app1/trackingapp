using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Domain.Entities;
using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;

        public GenericRepository(IDbConnectionFactory dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<int> AddAsync(T entity)
        {
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns);
            var stringOfParameters = string.Join(", ", columns.Select(e => "@" + e));

            var query = $"INSERT INTO {typeof(T).Name}s ({stringOfColumns}) VALUES ({stringOfParameters})";

            return await _dbConnectionFactory.ExecuteAsync<T>(query, entity);
        }

        public async Task<int> DeleteAsync(int id)
        {
            var query = $"DELETE FROM {typeof(T).Name}s WHERE Id = {@id}";
            
            return await _dbConnectionFactory.DeleteAsync(query, id);
        }
        

        public async Task<List<T>> QueryAsync()
        {
            var query = $"SELECT * FROM {typeof(T).Name}s ";

            return await _dbConnectionFactory.QueryAsync<T>(query);
        }

        public async Task<T> QueryFirstAsync(int id)
        {
            var query = $"SELECT * FROM {typeof(T).Name}s WHERE Id = {@id}";

            return await _dbConnectionFactory.QueryFirstAsync<T>(query);
        }

        public async Task<int> UpdateAsync(T entity)
        {
            var columns = GetColumns();
            var stringOfColumns = string.Join(", ", columns.Select(e => $"{e} = @{e}"));
            var query = $"UPDATE {typeof(T).Name}s SET {stringOfColumns} WHERE Id = @Id";

            return await _dbConnectionFactory.ExecuteAsync<T>(query, entity);
        }

        private IEnumerable<string> GetColumns()
        {
            return typeof(T)
                    .GetProperties()
                    .Where(e => e.Name != "Id" && !e.PropertyType.GetTypeInfo().IsGenericType)
                    .Select(e => e.Name);
        }
    }
}