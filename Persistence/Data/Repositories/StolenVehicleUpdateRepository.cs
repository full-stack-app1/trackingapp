using Domain.Entities;
using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class StolenVehicleUpdateRepository : GenericRepository<StolenVehicleUpdate>, IStolenVehicleUpdateRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public StolenVehicleUpdateRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }
    }
}