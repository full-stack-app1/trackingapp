using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IAppUserRepository appUserRepository, IVehicleRepository vehicleRepository, IStolenVehicleRepository stolenVehicleRepository, 
            IStolenVehicleUpdateRepository stolenVehicleUpdateRepository, IRecoveredVehicleRepository recoveredVehicleRepository)
        {
            AppUsers = appUserRepository;
            Vehicles = vehicleRepository;
            StolenVehicles = stolenVehicleRepository;
            StolenVehicleUpdates = stolenVehicleUpdateRepository;
            RecoveredVehicles = recoveredVehicleRepository;
        }

        public IAppUserRepository AppUsers { get; set; }
        public IVehicleRepository Vehicles { get; set; }
        public IStolenVehicleRepository StolenVehicles { get; set; }
        public IStolenVehicleUpdateRepository StolenVehicleUpdates { get; set; }
        public IRecoveredVehicleRepository RecoveredVehicles { get; set; }
    }
}