using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Models;
using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class RecoveredVehicleRepository : GenericRepository<RecoveredVehicle>, IRecoveredVehicleRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public RecoveredVehicleRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<List<RecoveredVehicleModel>> GetAllRecoveredVehicles()
        {
            var query = $"SELECT * FROM get_recovered_vehicles()";

            return await _dbConnectionFactory.QueryAsync<RecoveredVehicleModel>(query);
        }

        public async Task<RecoveredVehicleModel> GetRecoveredVehicleDetailsByNumberPlate(string numberPlate)
        {
            var query = $"SELECT * FROM get_recovered_vehicle_by_numberplate('{@numberPlate}')";

            return await _dbConnectionFactory.QueryFirstAsync<RecoveredVehicleModel>(query);
        }
    }
}