using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.ViewModels;
using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class StolenVehicleRepository : GenericRepository<StolenVehicle>, IStolenVehicleRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public StolenVehicleRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<StolenVehicleModel> GetStolenVehicleDetailsByNumberPlate(string numberPlate)
        {
            var query = $"SELECT * FROM get_stolen_vehicle_by_numberplate('{@numberPlate}')";

            return await _dbConnectionFactory.QueryFirstAsync<StolenVehicleModel>(query);
        }

        public async Task<List<StolenVehicleModel>> GetAllStolenVehicles()
        {
            var query = $"SELECT * FROM get_all_stolen_vehicles()";

            return await _dbConnectionFactory.QueryAsync<StolenVehicleModel>(query);
        }
    }
}