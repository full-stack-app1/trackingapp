using System.Threading.Tasks;
using Domain.Entities;
using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class AppUserRepository : GenericRepository<AppUser>, IAppUserRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public AppUserRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<AppUser> FindUserAsync(string username)
        {
            var query = $"SELECT * FROM {typeof(AppUser).Name}s WHERE username = '{@username}'";

            return await _dbConnectionFactory.QueryFirstAsync<AppUser>(query);
        }
    }
}