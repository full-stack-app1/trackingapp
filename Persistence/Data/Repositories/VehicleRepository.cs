using System.Threading.Tasks;
using Domain.Entities;
using Persistence.Data.Interfaces;

namespace Persistence.Data.Repositories
{
    public class VehicleRepository : GenericRepository<Vehicle>, IVehicleRepository
    {
        private readonly IDbConnectionFactory _dbConnectionFactory;
        public VehicleRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {
            _dbConnectionFactory = dbConnectionFactory;
        }

        public async Task<Vehicle> GetVehicleByNumberPlateAsync(string numberPlate)
        {
            var query = $"SELECT * FROM Vehicles WHERE numberplate = '{@numberPlate}'";

            return await _dbConnectionFactory.QueryFirstAsync<Vehicle>(query);
        }
    }
}