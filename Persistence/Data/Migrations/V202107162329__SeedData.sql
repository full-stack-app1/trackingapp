-- Insert into owners table

INSERT INTO "owners" ("id", "firstname", "lastname", "gender", "phonenumber", "email", "registrationdate") VALUES
(1, 'Bob', 'Bobbity', 'Male', '0742531244', 'bob@test.com', '2020-10-09');

INSERT INTO "owners" ("id", "firstname", "lastname", "gender", "phonenumber", "email", "registrationdate") VALUES
(2, 'Kelly', 'Rose', 'Female', '0723568694', 'kelly@test.com', '2020-02-23');

INSERT INTO "owners" ("id", "firstname", "lastname", "gender", "phonenumber", "email", "registrationdate") VALUES
(3, 'John', 'Doe', 'Male', '0714523689', 'john@test.com', '2021-03-11');

-- Insert into vehicles table

INSERT INTO "vehicles" ("id", "ownerid", "guidid", "brand", "model", "mileage", "color", "fueltype", "transmission", "vin", "enginenumber", "numberplate", "description", "registrationdate") VALUES
(1,	3,	'481799af-670a-434c-ae9b-8f3291e7d251',	'Toyota',	'Yaris',	'25000 mil',	'Red',	'Gasoline',	'Manual',	'4Y1SL65848Z411439',	'PJ12345U123456P',	'YH21 JCT',	'Car was bought in 2008',	'2021-07-01');

INSERT INTO "vehicles" ("id", "ownerid", "guidid", "brand", "model", "mileage", "color", "fueltype", "transmission", "vin", "enginenumber", "numberplate", "description", "registrationdate") VALUES
(2,	1,	'841a667a-5920-48d2-bde5-77e4d321fd1c',	'VW',	'Polo',	'32600 mil',	'White',	'Petrol',	'Automatic',	'4Y1SL65848Z411439',	'PJ12345U123456P',	'YH21 JCT',	'Car has a black bumper',	'2020-11-01');



