--change table name
ALTER TABLE "owners" 
	RENAME TO "appusers";

-- Add columns to appusers
ALTER TABLE "appusers"
	ADD "username" text NULL,
	ADD "passwordhash" bytea NULL,
	ADD "passwordsalt" bytea NULL;


--ALTER Vehicle Table guid type
ALTER TABLE "vehicles"
	ALTER COLUMN "guidid" TYPE uuid USING guidid::uuid;