-- Insert into StolenVehicle table

INSERT INTO "stolenvehicles" ("id", "vehicleid", "location", "datestolen", "description") VALUES
(1, 9, 'Durban, Florida', '2021-08-13', 'Vehicle does not have a tracker');

INSERT INTO "stolenvehicles" ("id", "vehicleid", "location", "datestolen", "description") VALUES
(2, 2, 'Johannesburg, CBD', '2021-08-09', 'Vehicle was taken by 3 men');
