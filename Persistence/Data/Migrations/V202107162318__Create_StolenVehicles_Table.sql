DROP TABLE IF EXISTS "stolenvehicles";
DROP SEQUENCE IF EXISTS stolenvehicles_id_seq;
CREATE SEQUENCE stolenvehicles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."stolenvehicles" (
	"id" integer DEFAULT nextval ('stolenvehicles_id_seq') NOT NULL,
	"vehicleid" integer NOT NULL,
	"location" text NOT NULL,
	"datestolen" date NOT NULL,
	"description" text NULL,
	CONSTRAINT "stolenvehicles_pkey" PRIMARY KEY ("id"),
	CONSTRAINT "stolenvehicles_fkey" FOREIGN KEY ("vehicleid") REFERENCES vehicles ("id")
) WITH (oids = false);
