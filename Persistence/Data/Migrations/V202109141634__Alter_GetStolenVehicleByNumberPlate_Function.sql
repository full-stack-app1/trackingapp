CREATE OR REPLACE FUNCTION get_stolen_vehicle_by_numberplate(numberplatesearch text) RETURNS 
TABLE(brand text, model text, color text, vin text, enginenumber text, numberplate text, location text, datestolen date, description text, firstname text, lastname text)

AS $$

 BEGIN

  RETURN QUERY
  SELECT V.brand, V.model, V.color, V.vin, V.enginenumber, V.numberplate, SV.location, SV.datestolen, SV.description, AU.firstname, AU.lastname
  FROM stolenvehicles SV
  INNER JOIN vehicles V ON SV.vehicleid = V.id
  INNER JOIN appusers AU ON V.appuserid = AU.id
  WHERE V.numberplate = numberplatesearch
  ORDER BY SV.datestolen DESC;
  
 END;

$$ LANGUAGE plpgsql;