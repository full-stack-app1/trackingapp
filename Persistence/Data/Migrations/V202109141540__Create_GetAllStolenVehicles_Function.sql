CREATE OR REPLACE FUNCTION get_all_stolen_vehicles() RETURNS 
TABLE(brand text, model text, color text, vin text, enginenumber text, numberplate text, location text, datestolen date, description text, firstname text, lastname text)

AS $$

 BEGIN

  CREATE TEMP TABLE stolenvehicle AS
  SELECT V.brand, V.model, V.color, V.vin, V.enginenumber, V.numberplate, SV.location, SV.datestolen, SV.description, AU.firstname, AU.lastname
  FROM stolenvehicles SV
  INNER JOIN vehicles V ON SV.vehicleid = V.id
  INNER JOIN appusers AU ON V.appuserid = AU.id
  ORDER BY SV.datestolen DESC;

  RETURN QUERY SELECT * FROM stolenvehicle; 
  
 END;

$$ LANGUAGE plpgsql;