DROP TABLE IF EXISTS "stolenvehiclesupdates";
DROP SEQUENCE IF EXISTS stolenvehiclesupdates_id_seq;
CREATE SEQUENCE stolenvehiclesupdates_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."stolenvehiclesupdates" (
	"id" integer DEFAULT nextval ('stolenvehiclesupdates_id_seq') NOT NULL,
	"stolenvehicleid" integer NOT NULL,
	"vehicleid" integer NOT NULL,
	"location" text NOT NULL,
	"comment" text NOT NULL,
	"datecommentadded" date NOT NULL,
	CONSTRAINT "stolenvehiclesupdates_pkey" PRIMARY KEY ("id"),
	CONSTRAINT "stolenvehiclesupdates_fkey" FOREIGN KEY ("stolenvehicleid") REFERENCES stolenvehicles ("id")
) WITH (oids = false);
