CREATE OR REPLACE FUNCTION get_recovered_vehicle_by_numberplate(numberplatesearch text) RETURNS 
TABLE(brand text, model text, color text, vin text, enginenumber text, numberplate text, location text, daterecovered date, description text, firstname text, lastname text)

AS $$

 BEGIN

  RETURN QUERY
    SELECT v.brand, v.model, v.color, v.vin, v.enginenumber, v.numberplate, rv.location, rv.daterecovered, rv.description, au.firstname, au.lastname
    FROM recoveredvehicles rv
    INNER JOIN vehicles v 
    ON rv.vehicleid = v.id
    INNER JOIN appusers au 
    ON v.appuserid = au.id
    WHERE v.numberplate = numberplatesearch;
  
 END;

$$ LANGUAGE plpgsql;