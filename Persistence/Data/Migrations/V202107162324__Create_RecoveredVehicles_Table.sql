DROP TABLE IF EXISTS "recoveredvehicles";
DROP SEQUENCE IF EXISTS recoveredvehicles_id_seq;
CREATE SEQUENCE recoveredvehicles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."recoveredvehicles" (
	"id" integer DEFAULT nextval ('recoveredvehicles_id_seq') NOT NULL,
	"stolenvehicleid" integer NOT NULL,
	"vehicleid" integer NOT NULL,
	"location" text NOT NULL,
	"daterecovered" date NOT NULL,
	"description" text NULL,
	CONSTRAINT "recoveredvehicles_pkey" PRIMARY KEY ("id"),
	CONSTRAINT "recoveredvehicles_fkey" FOREIGN KEY ("stolenvehicleid") REFERENCES stolenvehicles ("id")
) WITH (oids = false);