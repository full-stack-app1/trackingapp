DROP TABLE IF EXISTS "owners";
DROP SEQUENCE IF EXISTS owners_id_seq;
CREATE SEQUENCE owners_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."owners" (
	"id" integer DEFAULT nextval('owners_id_seq') NOT NULL,
	"firstname" text NOT NULL,
	"lastname" text NOT NULL,
	"gender" text NOT NULL,
	"phonenumber" text NOT NULL,
	"email" text NULL,
	"registrationdate" date NOT NULL,
	CONSTRAINT "owners_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
