DROP TABLE IF EXISTS "vehicles";
DROP SEQUENCE IF EXISTS vehicles_id_seq;
CREATE SEQUENCE vehicles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."vehicles" (
    "id" integer DEFAULT nextval('vehicles_id_seq') NOT NULL,
    "ownerid" integer NOT NULL,
    "guidid" text NOT NULL,
    "brand" text NOT NULL,
    "model" text NOT NULL,
    "mileage" text NOT NULL,
    "color" text NOT NULL,
    "fueltype" text NOT NULL,
    "transmission" text NOT NULL,
	"engine" double precision,
    "vin" text NOT NULL,
    "enginenumber" text NOT NULL,
    "numberplate" text NOT NULL,
    "description" text,
    "registrationdate" date NOT NULL,
    CONSTRAINT "vehicles_pkey" PRIMARY KEY ("id"),
	CONSTRAINT "vehicles_fkey" FOREIGN KEY ("ownerid") REFERENCES owners ("id")
) WITH (oids = false);