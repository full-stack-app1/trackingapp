using System.Text;
using Application.Core;
using Application.Services.AppUsers;
using Application.Services.Encryptions;
using Application.Services.RecoveredVehicles;
using Application.Services.StolenVehicles;
using Application.Services.Token;
using Application.Services.Vehicles;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Persistence.Data;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
            });

            services.AddPersistence();
            services.AddCors(opt => 
            {
                opt.AddPolicy("CorsPolicy", policy => 
                {
                    policy.AllowAnyMethod().AllowAnyHeader().WithOrigins("http://localhost:3000");
                });
            });

            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IEncryptionProvider, EncryptionProvider>();
            services.AddScoped<IAppUserService, AppUserService>();
            services.AddScoped<IVehicleService, VehicleService>();
            services.AddScoped<IStolenVehicleService, StolenVehicleService>();
            services.AddScoped<IRecoveredVehicleService, RecoveredVehicleService>();
            
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => 
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config[Constants.Key])),
                        ValidateIssuer = false,
                        ValidateAudience = false,

                    };
                });

            return services;
        }
    }
}