using System.Threading.Tasks;
using Application.Services.StolenVehicles;
using Domain.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Authorize]
    public class StolenVehiclesController : BaseApiController
    {
        private IStolenVehicleService _stolenVehicleService;
        public StolenVehiclesController(IStolenVehicleService stolenVehicleService)
        {
            _stolenVehicleService = stolenVehicleService;
        }

        protected IStolenVehicleService StolenVehicleService => _stolenVehicleService ??= HttpContext.RequestServices.GetService<IStolenVehicleService>();

        [HttpGet]
        public async Task<IActionResult> GetAllStolenVehicles()
        {
            return HandleResult(await StolenVehicleService.GetStolenVehiclesAsync());
        }

        [HttpGet("{numberPlate}")]
        public async Task<IActionResult> GetStolenVehicleByNumberPlate(string numberPlate)
        {
            return HandleResult(await StolenVehicleService.GetStolenVehicleByNumberPlate(numberPlate));
        }

        [HttpPost]
        public async Task<IActionResult> ReportStolenVehicle(StolenVehicle stolenVehicle)
        {
            return HandleResult(await StolenVehicleService.ReportStolenVehicleAsync(stolenVehicle));
        }
    }
}