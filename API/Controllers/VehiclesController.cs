﻿using System.Threading.Tasks;
using Application.Services.Vehicles;
using Domain.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    //[Authorize]
    public class VehiclesController : BaseApiController
    {
        private IVehicleService _vehicleService;
        public VehiclesController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }
        protected IVehicleService VehicleService => _vehicleService ??= HttpContext.RequestServices.GetService<IVehicleService>();


        [HttpGet]
        public async Task<IActionResult> GetAllVehicles()
        {
            return HandleResult(await VehicleService.GetAllVehiclesAsync());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetVehicle(int id)
        {
            return HandleResult(await VehicleService.GetVehicleByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateVehicle(Vehicle vehicle)
        {
            return HandleResult(await VehicleService.CreateVehicleAsync(vehicle));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditVehicle(int id, Vehicle vehicle)
        {
            vehicle.Id = id;
            return HandleResult(await VehicleService.UpdateVehicleAsync(vehicle));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVehicle(int id)
        {
            return HandleResult(await VehicleService.DeleteVehicleAsync(id));
        }
    }
}
