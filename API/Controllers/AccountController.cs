using System.Threading.Tasks;
using Application.Services.AppUsers;
using Microsoft.Extensions.DependencyInjection;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Application.DTOs;

namespace API.Controllers
{
    public class AccountController : BaseApiController
    {
        private IAppUserService _appUserService;
        public AccountController(IAppUserService appUserService)
        {
            _appUserService = appUserService;
        }

        protected IAppUserService AppUserService => _appUserService ??= HttpContext.RequestServices.GetService<IAppUserService>();

        [HttpPost("register")]
        public async Task<ActionResult<AppUser>> Register(RegisterDto registerDto)
        {
            return HandleResult(await AppUserService.Register(registerDto));
        }

        [HttpPost("login")]
        public async Task<ActionResult<AppUser>> Login(LoginDto loginDto)
        {
            return HandleResult(await AppUserService.Login(loginDto));
        }
    }
}