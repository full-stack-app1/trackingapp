import React from 'react';
import { Button, Container, Menu } from 'semantic-ui-react';

interface NavBarComponentProps {
    openForm: () => void;
}

const NavBarComponent: React.FC<NavBarComponentProps> = ({openForm}) => {
    return (
        <Menu inverted fixed='top'>
            <Container>
                <Menu.Item header>
                    <img src="/assets/logo.png" alt="logo" style={{marginRight: '10px'}} />
                    TrackingApp
                </Menu.Item>
                <Menu.Item name='Vehicles' />
                <Menu.Item>
                    <Button onClick={openForm} positive content='Add Vehicle' />
                </Menu.Item>
            </Container>
        </Menu>
    )
}

export default NavBarComponent;