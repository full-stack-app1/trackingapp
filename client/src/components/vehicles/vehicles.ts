import axios from "axios";
import requests from "../../api/agent"
import { Vehicle } from "../../features/vehicles/types"

const Vehicles = {
    list: () => requests.get<Vehicle[]>('/vehicles'),
    details: (id: string) => requests.get<Vehicle>(`/vehicles/${id}`),
    create: (vehicle: Vehicle) => requests.post<void>('/vehicles', vehicle),
    update: (vehicle: Vehicle) => axios.put<void>(`/vehicles/${vehicle.id}`, vehicle),
    delete: (id: number ) => axios.delete<void>(`/vehicles/${id}`)
}

const agent = {
    Vehicles
}

export default agent;

