import React from 'react';
import { Grid } from 'semantic-ui-react';
import VehicleDetails from '../details/VehicleDetails';
import VehicleForm from '../form/VehicleForm';
import { Vehicle } from '../types';
import VehicleList from './VehicleList';

interface VehicleDashBoardProps {
    vehicles: Vehicle[];
    selectedVehicle: Vehicle | undefined;
    selectVehicle: (id: string) => void;
    cancelSelectVehicle: () => void;
    editMode: boolean;
    openForm: (id: string) => void;
    closeForm: () => void;
    createOrEdit: (vehicle: Vehicle) => void;
    deleteVehicle: (id: number) => void;
    submitting: boolean;
}

const VehicleDashBoard: React.FC<VehicleDashBoardProps> = ({vehicles, selectedVehicle, deleteVehicle, selectVehicle, cancelSelectVehicle,
        editMode, openForm, closeForm, createOrEdit, submitting}) => {
    return (
        <Grid>
            <Grid.Column width='10'>
                <VehicleList vehicles={vehicles}
                 selectVehicle={selectVehicle} 
                 deleteVehicle={deleteVehicle}
                 submitting={submitting}
                />
            </Grid.Column>
            <Grid.Column width='6'>
                {selectedVehicle && !editMode &&
                <VehicleDetails 
                    vehicle={selectedVehicle} 
                    cancelSelectVehicle={cancelSelectVehicle}
                    openForm={openForm}
                />}
                {editMode &&
                <VehicleForm 
                    closeForm={closeForm} 
                    vehicle={selectedVehicle} 
                    createOrEdit={createOrEdit}
                    submitting={submitting}
                />}
            </Grid.Column>
        </Grid>
    )
}

export default VehicleDashBoard;