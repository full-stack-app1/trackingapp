import React, { SyntheticEvent, useState } from 'react';
import { Button, Item, Label, Segment } from 'semantic-ui-react';
import { Vehicle } from '../types';

interface VehicleListProps {
    vehicles: Vehicle[];
    selectVehicle: (id: string) => void;
    deleteVehicle: (id: number) => void;
    submitting: boolean;
}

const VehicleList: React.FC<VehicleListProps> = ({vehicles, selectVehicle, deleteVehicle, submitting }) => {
    const [target, setTarget] = useState('');

    function handleVehicleDelete(e: SyntheticEvent<HTMLButtonElement>, id: number) {
        setTarget(e.currentTarget.name);
        deleteVehicle(id);
    }

    return (
        <Segment>
            <Item.Group divided>
                {vehicles.map(vehicle => 
                    <Item key={vehicle.guidId}>
                        <Item.Content>
                            <Item.Header as='a'>{vehicle.brand}</Item.Header>
                            <Item.Meta>{vehicle.registrationDate}</Item.Meta>
                            <Item.Description>
                                <div>{vehicle.description}</div>
                                <div>{vehicle.model}, {vehicle.numberPlate}</div>
                            </Item.Description>
                            <Item.Extra>
                                <Button onClick={() => selectVehicle(vehicle.guidId)} floated='right' content='View' color='blue' />
                                <Button
                                    name={vehicle.guidId}
                                    loading={submitting && target === vehicle.guidId} 
                                    onClick={(e) => handleVehicleDelete(e, vehicle.id)} 
                                    floated='right' 
                                    content='Delete' 
                                    color='red' 
                                />
                                <Label basic content={vehicle.fuelType} />
                            </Item.Extra>
                        </Item.Content>
                    </Item>
                )}
            </Item.Group>
        </Segment>
    )
}

export default VehicleList;