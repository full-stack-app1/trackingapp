import React from 'react';
import { Button, Card, Image } from 'semantic-ui-react';
import { IMAGE_URL } from '../../../components/layout/constants';
import { Vehicle } from '../types';

interface VehicleDetailsProps {
    vehicle: Vehicle;
    cancelSelectVehicle: () => void;
    openForm: (id: string) => void;
}

const VehicleDetails: React.FC<VehicleDetailsProps> = ({vehicle, cancelSelectVehicle, openForm}) => {
  return (
    <Card fluid>
      <Image src={`${IMAGE_URL+vehicle.brand}.jpg`} />
      <Card.Content>
        <Card.Header>{vehicle.model}</Card.Header>
        <Card.Meta>
          <span>{vehicle.registrationDate}</span>
        </Card.Meta>
        <Card.Description>
          {vehicle.description}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
         <Button.Group widths='2'>
             <Button onClick={() => openForm(vehicle.guidId)} basic color='blue' content='Edit'/>
             <Button onClick={cancelSelectVehicle} basic color='grey' content='Cancel'/>
         </Button.Group>
      </Card.Content>
    </Card>
  );
}

export default VehicleDetails;