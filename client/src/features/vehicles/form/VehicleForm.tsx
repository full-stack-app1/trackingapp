import React, { ChangeEvent, useState } from 'react';
import { useForm } from 'react-hook-form';
import { Button, Form, Segment } from 'semantic-ui-react';
import { Vehicle } from '../types';

interface VehicleFormProps {
    vehicle: Vehicle | undefined;
    closeForm: () => void;
    createOrEdit: (vehicle: Vehicle) => void;
    submitting: boolean;
}

const VehicleForm: React.FC<VehicleFormProps> = ({vehicle: selectedVehicle, closeForm, createOrEdit, submitting}) => {

    const initialState = selectedVehicle ?? {
        id: 0,
        ownerId: 0,
        guidId: '',
        brand: '',
        model: '',
        fuelType: '',
        mileage: '',
        transmission: '',
        engine: 0,
        vin: '',
        engineNumber: '',
        numberPlate: '',
        color: '',
        description: '',
        registrationDate: ''
    }

    const { handleSubmit } = useForm<VehicleFormProps>();
    const [vehicle, setVehicle] = useState(initialState);

    function onSubmit() {
        createOrEdit(vehicle);
    }

    function handleInputChange(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        const {name, value} = event.target;
        setVehicle({...vehicle, [name]: value})
    }

    return (
        <Segment clearing>
            <Form onSubmit={handleSubmit(onSubmit)} autoComplete='off'>
                <Form.Input placeholder='Maker' value={vehicle.brand} name='brand' onChange={handleInputChange} />
                <Form.Input placeholder='Model' value={vehicle.model} name='model' onChange={handleInputChange} />
                <Form.Input placeholder='Fuel Type' value={vehicle.fuelType} name='fuelType' onChange={handleInputChange} />
                <Form.Input placeholder='Mileage' value={vehicle.mileage} name='mileage' onChange={handleInputChange} />
                <Form.Input placeholder='Transmission' value={vehicle.transmission} name='transmission' onChange={handleInputChange} />
                <Form.Input placeholder='VIN' value={vehicle.vin} name='vin' onChange={handleInputChange} />
                <Form.Input placeholder='Engine Number' value={vehicle.engineNumber} name='engineNumber' onChange={handleInputChange} />
                <Form.Input placeholder='Number Plate' value={vehicle.numberPlate} name='numberPlate' onChange={handleInputChange}  />
                <Form.Input placeholder='Color' value={vehicle.color} name='color' onChange={handleInputChange}  />
                <Form.TextArea placeholder='Description' value={vehicle.description} name='description' onChange={handleInputChange} />
                <Button loading={submitting} floated='right' positive type='submit' content='Submit'/>
                <Button onClick={closeForm} floated='right' type='button' content='Cancel'/>
            </Form>
        </Segment>
    )
}

export default VehicleForm;