export type Vehicle = {
    id: number
    ownerId: number;
    guidId: string;
    brand: string;
    model: string;
    mileage: string;
    color: string;
    fuelType: string;
    transmission: string;
    engine: number;
    vin: string;
    engineNumber: string;
    numberPlate: string;
    description: string;
    registrationDate: string;
}
