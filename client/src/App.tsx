import React, { useEffect, useState } from 'react';
import { Container } from 'semantic-ui-react';
import {v4 as uuid} from 'uuid';
import NavBar from './components/layout/NavBarComponent';
import LoadingComponent from './components/layout/LoadingComponent';
import VehicleDashBoard from './features/vehicles/dashboard/VehiclesDashboard';
import { Vehicle } from './features/vehicles/types';
import agent from './components/vehicles/vehicles';


function App() {
  const [vehicles, setVehicles] = useState<Vehicle[]>([]);
  const [selectedVehicle, setSelectedVehicle] = useState<Vehicle | undefined>(undefined);
  const [editMode, setEditMode] = useState(false);
  const [loading, setLoading] = useState(true);
  const [submitting, setSubmitting] = useState(false);

  useEffect(() => {
    agent.Vehicles.list().then(response => {
      let vehicles: Vehicle[] = [];
      response.forEach(vehicle => {
        vehicle.registrationDate = vehicle.registrationDate.split('T')[0];
        vehicles.push(vehicle);
      })
      setVehicles(vehicles);
      setLoading(false);
    })
  }, [])

  function handleSelectVehicle(id: string) {
    setSelectedVehicle(vehicles.find(x => x.guidId === id));
  }

  function handleCancelSelectVehicle() {
    setSelectedVehicle(undefined);
  }

  function handleFormOpen(id?: string) {
    id ? handleSelectVehicle(id) : handleCancelSelectVehicle();
    setEditMode(true);
  }

  function handleFormClose() {
    setEditMode(false);
  }

  function handleCreateOrEditVehicle(vehicle: Vehicle) {
    setSubmitting(true);
    if (vehicle.guidId) {
      agent.Vehicles.update(vehicle).then(() => {
        setVehicles([...vehicles.filter(x => x.id !== vehicle.id), vehicle])
        setSelectedVehicle(vehicle);
        setEditMode(false);
        setSubmitting(false);
      })
    } else {
      vehicle.guidId = uuid();
      vehicle.registrationDate = "2021-07-01";
      agent.Vehicles.create(vehicle).then(() => {
        setVehicles([...vehicles, vehicle])
        setSelectedVehicle(vehicle);
        setEditMode(false);
        setSubmitting(false);
      })
    }
  }

  function handleDeleteVehicle(id: number) {
    setSubmitting(true);
    agent.Vehicles.delete(id).then(() => {
      setVehicles([...vehicles.filter(x => x.id !== id)]);
      setSubmitting(false);
    })
  }

  if (loading) return <LoadingComponent content='Loading app' />

  return (
    <>
      <NavBar openForm={handleFormOpen} />
      <Container style={{marginTop: '7em'}}>
        <VehicleDashBoard 
          vehicles={vehicles}
          selectedVehicle={selectedVehicle}
          selectVehicle={handleSelectVehicle}
          cancelSelectVehicle={handleCancelSelectVehicle}
          editMode={editMode}
          openForm={handleFormOpen}
          closeForm={handleFormClose}
          createOrEdit={handleCreateOrEditVehicle}
          deleteVehicle={handleDeleteVehicle}
          submitting={submitting}
        />
      </Container>
      
    </>
  );
}

export default App;
